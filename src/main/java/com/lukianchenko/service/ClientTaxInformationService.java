package com.lukianchenko.service;

import com.lukianchenko.DAO.implementation.ClientTaxInformationDAOImpl;
import com.lukianchenko.model.ClientTaxInformationEntity;

import java.sql.SQLException;
import java.util.List;

public class ClientTaxInformationService {
    public List<ClientTaxInformationEntity> findAll() throws SQLException {
        return new ClientTaxInformationDAOImpl().findAll();
    }

    public ClientTaxInformationEntity findById(Integer id) throws SQLException {
        return new ClientTaxInformationDAOImpl().findById(id);
    }

    public int create(ClientTaxInformationEntity entity) throws SQLException {
        return new ClientTaxInformationDAOImpl().create(entity);
    }

    public int update(ClientTaxInformationEntity entity) throws SQLException {
        return new ClientTaxInformationDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new ClientTaxInformationDAOImpl().delete(id);
    }
}
