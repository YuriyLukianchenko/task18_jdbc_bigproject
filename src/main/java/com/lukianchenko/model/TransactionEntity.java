package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;

import java.time.DateTimeException;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "transactions")
public class TransactionEntity {
    @PrimaryKey
    @Column(name = "transaction_id", length = 20)
    Integer transactionId;
    @Column(name = "id_number_from", length = 11)
    Integer clientIdFrom;
    @Column(name = "id_number_to", length = 11)
    Integer clientIdTo;
    @Column(name = "suma", length = 11)
    Integer transactionAmount;
    @Column(name = "date")
    DateTimeException date;

    public TransactionEntity() {
    }

    public TransactionEntity(Integer transactionId, Integer clientIdFrom, Integer clientIdTo,
                             Integer transactionAmount, DateTimeException date) {
        this.transactionId = transactionId;
        this.clientIdFrom = clientIdFrom;
        this.clientIdTo = clientIdTo;
        this.transactionAmount = transactionAmount;
        this.date = date;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getClientIdFrom() {
        return clientIdFrom;
    }

    public void setClientIdFrom(Integer clientIdFrom) {
        this.clientIdFrom = clientIdFrom;
    }

    public Integer getClientIdTo() {
        return clientIdTo;
    }

    public void setClientIdTo(Integer clientIdTo) {
        this.clientIdTo = clientIdTo;
    }

    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public DateTimeException getDate() {
        return date;
    }

    public void setDate(DateTimeException date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return String.format("%-20d %-11d %-11d %-10d %-30", transactionId, clientIdFrom, clientIdTo,
                transactionAmount, date);
    }
}
