package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;

import java.math.BigDecimal;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "currencies")
public class CurrencyEntity {
    @PrimaryKey
    @Column(name = "id_currency", length = 11)
    private Integer currencyId;
    @Column(name = "name", length = 20)
    private String name;
    @Column(name = "kurs_to_usd", length = 10)
    private BigDecimal usdExchangeRate;

    public CurrencyEntity() {
    }

    public CurrencyEntity(Integer currencyId, String name, BigDecimal usdExchangeRate) {
        this.currencyId = currencyId;
        this.name = name;
        this.usdExchangeRate = usdExchangeRate;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUsdExchangeRate() {
        return usdExchangeRate;
    }

    public void setUsdExchangeRate(BigDecimal usdExchangeRate) {
        this.usdExchangeRate = usdExchangeRate;
    }

    @Override
    public String toString() {
        return String.format("%-11d %-20s %-10s", currencyId, name, usdExchangeRate);
    }
}
