package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;
import org.joda.time.DateTime;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "tax_administration")
public class ClientTaxInformationEntity {
    @PrimaryKey
    @Column(name = "id_number", length = 11)
    Integer clientId;
    @Column(name = "name", length = 20)
    String clientName;
    @Column(name = "surname", length = 20)
    String clientSurName;
    @Column(name = "birth_year", length = 11)
    Integer clientBirthYear;
    @Column(name = "registration_date")
    DateTime registrationDate;
    @Column(name = "info", length = 50)
    String clientInformation;

    public ClientTaxInformationEntity() {
    }

    public ClientTaxInformationEntity(Integer clientId, String clientName,
                                      String clientSurName, Integer clientBirthYear,
                                      DateTime registrationDate, String clientInformation) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.clientSurName = clientSurName;
        this.clientBirthYear = clientBirthYear;
        this.registrationDate = registrationDate;
        this.clientInformation = clientInformation;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientSurName() {
        return clientSurName;
    }

    public void setClientSurName(String clientSurName) {
        this.clientSurName = clientSurName;
    }

    public Integer getClientBirthYear() {
        return clientBirthYear;
    }

    public void setClientBirthYear(Integer clientBirthYear) {
        this.clientBirthYear = clientBirthYear;
    }

    public DateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(DateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getClientInformation() {
        return clientInformation;
    }

    public void setClientInformation(String clientInformation) {
        this.clientInformation = clientInformation;
    }

    @Override
    public String toString() {
        return String.format("%-11d %-20s %-11d %-50s %-50s", clientId, clientName,
                clientSurName, clientBirthYear, registrationDate, clientInformation );
    }
}
