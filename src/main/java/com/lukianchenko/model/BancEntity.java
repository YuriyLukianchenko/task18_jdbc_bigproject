package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;

import java.math.BigDecimal;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "bancs")
public class BancEntity {
    @PrimaryKey
    @Column(name = "banc_id", length = 11)
    Integer bancId;
    @Column(name = "name", length = 50)
    String name;
    @Column(name = "client_number", length = 11)
    Integer clientNumber;
    @Column(name = "balnce", length = 10) // 10? why then bigdecimal ?
    BigDecimal balance;
    @Column(name = "reg_number", length = 11)
    Integer registrationNumber;
    @Column(name = "client_quantity", length = 11)
    Integer clientAmount;
    @Column(name = "depo_quantity", length = 11)
    Integer depositAmount;
    @Column(name = "credit_quantity", length = 11)
    Integer creditAmount;

    public BancEntity() {
    }

    public BancEntity(Integer bancId, String name, Integer clientNumber,
                      BigDecimal balance, Integer registrationNumber,
                      Integer clientAmount, Integer depositAmount, Integer creditAmount) {
        this.bancId = bancId;
        this.name = name;
        this.clientNumber = clientNumber;
        this.balance = balance;
        this.registrationNumber = registrationNumber;
        this.clientAmount = clientAmount;
        this.depositAmount = depositAmount;
        this.creditAmount = creditAmount;
    }

    public Integer getBancId() {
        return bancId;
    }

    public void setBancId(Integer bancId) {
        this.bancId = bancId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Integer registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getClientAmount() {
        return clientAmount;
    }

    public void setClientAmount(Integer clientAmount) {
        this.clientAmount = clientAmount;
    }

    public Integer getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Integer depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Override
    public String toString() {
        return String.format("%-11d %-50s %-11d %-10s %-11d %-11d %-11d %-11d",
                bancId, name, clientNumber, balance, registrationNumber,
                clientAmount, depositAmount, creditAmount);
    }
}
