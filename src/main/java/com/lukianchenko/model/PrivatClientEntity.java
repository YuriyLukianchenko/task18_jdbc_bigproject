package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;

import java.math.BigDecimal;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "clients_privat")
public class PrivatClientEntity {
    @PrimaryKey
    @Column(name = "id_number", length = 11)
    Integer cLientID;
    @Column(name = "name", length = 20)
    String clientName;
    @Column(name = "surname", length = 20)
    String clientSurName;
    @Column(name = "birth_year", length = 11)
    Integer clientBirthYear;
    @Column(name = "number_of_depo", length = 11)
    Integer clientDepositAmount;
    @Column(name = "number_of_credit", length = 11)
    Integer clientCreditAmount;
    @Column(name = "balance", length = 10)
    BigDecimal clientBalance;

    public PrivatClientEntity() {
    }

    public PrivatClientEntity(Integer cLientID, String clientName, String clientSurName,
                              Integer clientBirthYear, Integer clientDepositAmount,
                              Integer clientCreditAmount, BigDecimal clientBalance) {
        this.cLientID = cLientID;
        this.clientName = clientName;
        this.clientSurName = clientSurName;
        this.clientBirthYear = clientBirthYear;
        this.clientDepositAmount = clientDepositAmount;
        this.clientCreditAmount = clientCreditAmount;
        this.clientBalance = clientBalance;
    }

    public Integer getcLientID() {
        return cLientID;
    }

    public void setcLientID(Integer cLientID) {
        this.cLientID = cLientID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientSurName() {
        return clientSurName;
    }

    public void setClientSurName(String clientSurName) {
        this.clientSurName = clientSurName;
    }

    public Integer getClientBirthYear() {
        return clientBirthYear;
    }

    public void setClientBirthYear(Integer clientBirthYear) {
        this.clientBirthYear = clientBirthYear;
    }

    public Integer getClientDepositAmount() {
        return clientDepositAmount;
    }

    public void setClientDepositAmount(Integer clientDepositAmount) {
        this.clientDepositAmount = clientDepositAmount;
    }

    public Integer getClientCreditAmount() {
        return clientCreditAmount;
    }

    public void setClientCreditAmount(Integer clientCreditAmount) {
        this.clientCreditAmount = clientCreditAmount;
    }

    public BigDecimal getClientBalance() {
        return clientBalance;
    }

    public void setClientBalance(BigDecimal clientBalance) {
        this.clientBalance = clientBalance;
    }

    @Override
    public String toString() {
        return String.format("%-11d %-20s %-20s %-11d %-11d %-11d %-10s", cLientID, clientName,
                clientSurName, clientBirthYear, clientDepositAmount, clientCreditAmount, clientBalance);
    }
}
