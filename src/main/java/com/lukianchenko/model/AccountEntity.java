package com.lukianchenko.model;

import com.lukianchenko.model.Annotation.Column;
import com.lukianchenko.model.Annotation.PrimaryKey;
import com.lukianchenko.model.Annotation.Table;
import org.apache.logging.log4j.core.tools.picocli.CommandLine;

import java.math.BigDecimal;

/**
 * Using for storing information from sql table.
 * @author Yura Lukianchenko
 * @version 1.0
 *
 */
@Table(name = "accounts")
public class AccountEntity {
    @PrimaryKey
    @Column(name = "account_id", length = 11)
    Integer accountId;
    @Column(name = "type", length = 10)
    String typeOfAccount;
    @Column(name = "id_number", length = 11)
    Integer clientId;
    @Column(name = "balance", length = 10)
    BigDecimal accountBalance;
    @Column(name = "rate", length = 11)
    Integer rate;
    @Column(name = "currency_type", length = 10)
    String currencyType;

    public AccountEntity() {
    }

    public AccountEntity(Integer accountId, String typeOfAccount, Integer clientId,
                         BigDecimal accountBalance, Integer rate, String currencyType) {
        this.accountId = accountId;
        this.typeOfAccount = typeOfAccount;
        this.clientId = clientId;
        this.accountBalance = accountBalance;
        this.rate = rate;
        this.currencyType = currencyType;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getTypeOfAccount() {
        return typeOfAccount;
    }

    public void setTypeOfAccount(String typeOfAccount) {
        this.typeOfAccount = typeOfAccount;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    @Override
    public String toString() {
        return String.format("%-11d %-10s %-11d %-10s %-11d %-10s", accountId, typeOfAccount, clientId,
                accountBalance, rate, currencyType);
    }
}
