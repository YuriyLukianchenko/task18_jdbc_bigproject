package com.lukianchenko;

import com.lukianchenko.model.ClientTaxInformationEntity;
import org.joda.time.DateTime;
import viewAndController.View;

import java.sql.SQLException;

public class Application {

    public static void main(String[] args) throws SQLException {

        new View().start();
    }
}
