package com.lukianchenko.DAO;

import com.lukianchenko.model.ClientTaxInformationEntity;

public interface ClientTaxInformationDAO extends GeneralDAO<ClientTaxInformationEntity, Integer> {

}
