package com.lukianchenko.DAO.implementation;

import com.lukianchenko.DAO.ClientTaxInformationDAO;
import com.lukianchenko.model.ClientTaxInformationEntity;
import com.lukianchenko.persistant.ConnectionManager;
import com.lukianchenko.transformer.Transformer;
import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

import java.net.ConnectException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientTaxInformationDAOImpl implements ClientTaxInformationDAO {
    private static final String FIND_ALL = "SELECT * FROM tax_administration";
    private static final String DELETE = "DELETE FROM tax_administration WHERE id_number=?";
    private static final String CREATE ="INSERT INTO tax_administration (id_number, name, surname, birth_year, registration_date, info) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE ="UPDATE tax_administration  SET name=?, surname=?, birth_year=?, registration_date=?, info=? WHERE id_number=?";
    private static final String FIND_BY_ID = "SELECT * FROM tax_administration WHERE id_number=?";

    @Override
    public List<ClientTaxInformationEntity> findAll() throws SQLException {
        List<ClientTaxInformationEntity> clientsTaxInfo = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()){
            try(ResultSet rs = statement.executeQuery(FIND_ALL)){
                while(rs.next()){
                    clientsTaxInfo.add((ClientTaxInformationEntity) new Transformer(ClientTaxInformationEntity.class).fromResultSetToEntity(rs));
                }

            }
        }
        return clientsTaxInfo;
    }

    @Override
    public ClientTaxInformationEntity findById(Integer id) throws SQLException {
        ClientTaxInformationEntity clientTaxInformationEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)){
            ps.setString(1, String.valueOf(id));
            try(ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    clientTaxInformationEntity = (ClientTaxInformationEntity) new Transformer<>(ClientTaxInformationEntity.class).fromResultSetToEntity(rs);
                    break;
                }
            }
        }

        return clientTaxInformationEntity;
    }

    @Override
    public int create(ClientTaxInformationEntity entity) throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        try(PreparedStatement ps = conn.prepareStatement(CREATE)){
            ps.setString(1,String.valueOf(entity.getClientId()));
            ps.setString(2,String.valueOf(entity.getClientName()));
            ps.setString(3,String.valueOf(entity.getClientSurName()));
            ps.setString(4,String.valueOf(entity.getClientBirthYear()));
            ps.setString(5,"2019-06-08 21:25:46");
            ps.setString(6,entity.getClientInformation());
            return ps.executeUpdate();

        }
    }

    @Override
    public int update(ClientTaxInformationEntity entity) throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        try(PreparedStatement ps = conn.prepareStatement(UPDATE)){
            ps.setString(6,String.valueOf(entity.getClientId()));
            ps.setString(1,String.valueOf(entity.getClientName()));
            ps.setString(2,String.valueOf(entity.getClientSurName()));
            ps.setString(3,String.valueOf(entity.getClientBirthYear()));
            ps.setString(4,String.valueOf(entity.getRegistrationDate()));
            ps.setString(5,entity.getClientInformation());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        try(PreparedStatement ps = conn.prepareStatement(DELETE)){
            ps.setString(1,String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
