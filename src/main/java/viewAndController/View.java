package viewAndController;

import com.lukianchenko.model.ClientTaxInformationEntity;
import com.lukianchenko.service.ClientTaxInformationService;
import org.joda.time.DateTime;


import java.sql.Date;
import java.sql.SQLException;

public class View {
    public void start() throws SQLException {
        ClientTaxInformationService service = new ClientTaxInformationService();
        ClientTaxInformationEntity entity = new ClientTaxInformationEntity(11, "Jerard",
                "Boxman", 1972,  DateTime.now(),"no information");
        service.create(entity);
    }
}
