package com.lukianchenko.persistant;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestConnectionManager {
    private static Connection conn = null;
    private static Connection myConn = null;
    boolean connectionIsClosed = true;

    @Test
    public void testGetConnection(){
        try{
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Bancs_sql_for_JDBC?serverTimezone=UTC&useSSL=false",
                    "root","529MysqLmysqL529AbETKa");
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
        myConn = ConnectionManager.getConnection();
        try{
            connectionIsClosed = myConn.isClosed();
        }
        catch(SQLException e){
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
        assertEquals(false,connectionIsClosed);
       // assertEquals(myConn,conn);
    }
}
